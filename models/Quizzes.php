<?php

namespace kllakk\quizzes\models;


class Quizzes extends \kllakk\quizzes\models\base\Quizzes
{
    public function getQList()
    {
        return implode('', array_map(function($question) { return "{$question->question_id}Q"; }, $this->questions));
    }

    public function getQuestions()
    {
        return $this->hasMany(Questions::class, ['quiz_id' => 'quiz_id']);
    }

    public function getConditions()
    {
        return $this->hasMany(Conditions::class, ['quiz_id' => 'quiz_id']);
    }

    public function getSettings()
    {
        return json_decode($this->quiz_settings);
    }

    public function getContactsText()
    {
        $settings = $this->getSettings();
        return $settings ? $settings->contactsText : null;
    }
}
