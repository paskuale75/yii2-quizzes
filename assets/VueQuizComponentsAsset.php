<?php

namespace kllakk\quizzes\assets;

use yii\web\AssetBundle;

class VueQuizComponentsAsset extends AssetBundle
{
    public $depends = [
        'kllakk\quizzes\assets\FontAwesomeAsset',
        'kllakk\quizzes\assets\BootstrapVueAsset',
    ];

    public $sourcePath = __DIR__ . '/components';

    public $js = [
        'Conditions.vue.js',
        'QuestionUpdate.vue.js',
        'Questions.vue.js',
        'QuizNew.vue.js',
        'QuizUpdate.vue.js',
        'Results.vue.js',
        'Quizzes.vue.js',
        'App.vue.js',
        'init.js',
    ];
}
