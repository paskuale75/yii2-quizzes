<?php

namespace kllakk\quizzes\assets;

use yii\web\AssetBundle;

class BootstrapVueAsset extends AssetBundle
{
    public $depends = [
        'kllakk\quizzes\assets\VueAsset',
    ];

    public $css = [
        'https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css',
        'https://unpkg.com/bootstrap-vue@2.21.2/dist/bootstrap-vue.css',
    ];

    public $js = [
        'https://www.unpkg.com/sortablejs@1.13.0/Sortable.min.js',
        'https://unpkg.com/bootstrap@4.5.0/dist/js/bootstrap.min.js',
        'https://unpkg.com/bootstrap-vue@2.21.2/dist/bootstrap-vue.js'
    ];
}
