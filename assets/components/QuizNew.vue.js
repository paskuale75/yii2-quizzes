const QuizNew = {
  template: `
      <b-modal id="modal-quiz-new" centered size="lg"
               ok-title="Создать" cancel-title="Отменить"
               @ok="create"
               title="Новый опрос">
          <p class="my-4">
              <!-- :state="validation" -->
              <b-input type="text" v-model="name" />
          </p>
      </b-modal>  
    `,
  methods: {
    create() {
      let params = {
        quiz_name: this.name,
        quiz_settings: this.QuizSettingsDefault,
      };

      $.post(this.routes['quizzes/create'], params).done((response) => {
        this.$emit('created');
        this.name = null;
      });
    }
  },
  data: () => {
    return {
      name: null,
      QuizSettingsDefault: {
        emails: '', // 'dispatcher@rfn.spb.ru,ivan@rfn.spb.ru',
        subject: 'Заявка с сайта',
        contactsText: '<p>Оставьте Ваш контакт и получите подборку квартир без риска столкнуться с мошенничеством и недостроем.</p>',
        resultsText: 'Спасибо!',
        result_button_text: 'Получить результат',
        QATemplate: '<p>%QUESTION%: %USER_ANSWER%</p>',
        resultsTemplate: 'Клиент: %USER_NAME%\n' +
          'Телефон: %USER_PHONE%\n' +
          '\n' +
          'Дата: %CURRENT_DATE%\n' +
          '%QUESTIONS_ANSWERS%',
      }
    }
  },
  computed: {
    validation() {
      return this.name ? this.name.length > 3 : false;
    }
  },
  props: ['routes']
};
