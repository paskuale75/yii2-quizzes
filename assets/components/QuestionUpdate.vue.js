const QuestionUpdate = {
  template: `
<div>
        <b-modal @show="show" :id="modalId"  centered size="md" :hide-footer="hideFooter"
                 ok-title="Сохранить" cancel-title="Отменить"
                 @ok="update"
                 :title="modalTitle">
            <div v-if="question">
                <p class="mb-4">
                    <!-- :state="validation" -->
                    <b-input type="text" v-model="question.question_name" />
                </p>
                <p>
                    <b-form-select v-model="typeId" :options="types"></b-form-select>
                </p>
                <b-container fluid class="px-0">
                    <b-row>
                        <b-col cols="9">Ответы</b-col>
                        <b-col cols="1" class="text-center px-0">Свой</b-col>
                        <b-col cols="2"></b-col>
                    </b-row>
                    <b-row v-for="(answer, index) in answers" :key="index" class="mb-2">
                        <b-col cols="9">
                            <b-input-group>
                                <b-input-group-prepend>
                                    <label :class="'image-container btn btn-secondary ' + (answer.AnswerImageUrl ? 'p-0' : '')">
                                        <div v-show="answer.AnswerImageUrl">
                                            <b-img :src="answer.AnswerImageUrl" fluid style="width: 38px; height: 36px" />
                                        </div>
                                        <div v-show="!answer.AnswerImageUrl">
                                            <i class="far fa-image"></i>
                                        </div>
                                        <div class="d-none">
                                            <b-form-file @change="onAnswerImageChange" :data-index="index" v-model="answer.AnswerImage" accept="image/*" plain></b-form-file>
                                        </div>
                                    </label>
                                </b-input-group-prepend>
                                <b-form-input v-model="answer.AnswerText" />
                            </b-input-group>
                        </b-col>
                        <b-col cols="1" class="text-center px-0 py-1">
                            <b-form-radio name="own" v-model="ownSelected" :value="index"></b-form-radio>
                        </b-col>
                        <b-col cols="2" class="text-right">
                            <b-button @click="deleteAnswer(index)"><i class="far fa-trash-alt"></i></b-button>
                        </b-col>
                    </b-row>
                    <b-row>
                        <b-col>
                            <b-button variant="link" class="px-0" @click="insertAnswer">Добавить новый ответ</b-button>
                        </b-col>
                    </b-row>
                </b-container>
            </div>
        </b-modal>
    </div>
    `,
  methods: {
    show() {
      this.get()
    },
    get() {
      if (this.id) {
        let params = {
          id: this.id,
        };

        $.getJSON(this.routes['questions/get'], params).done((response) => {
          this.question = response;
          this.typeId = this.question.question_type;
          this.answers = this.question.answer_array ? JSON.parse(this.question.answer_array) : [];
          this.ownSelected = this.answers.findIndex(answer => answer.AnswerOwn == true);
        });
      }
    },
    onAnswerImageChange(e) {
      let index = e.target.getAttribute('data-index');
      let file = e.target.files[0];
      let answer = this.answers.find((answer, i) => { return i == index; });
      if (answer) {
        answer.AnswerImageUrl = URL.createObjectURL(file);
      }
    },
    update() {
      let params = {
        quiz_id: this.question.quiz_id,
        question_name: this.question.question_name,
        question_type: this.typeId,
        answer_array: this.answers
      };

      let formData = new FormData();

      this.answers.forEach((answer, index) => {
        let file = answer.AnswerImage;
        if (file) {
          formData.append(`file-${index}[]`, file, file.name);
        }
      });

      formData.append('form', JSON.stringify(params));

      $.ajax({
        type: 'POST',
        enctype: 'multipart/form-data',
        url: (this.id ? this.routes['questions/update'].replace('id=id', `id=${this.id}`) : this.routes['questions/create']),
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: (response) => {
          this.$emit(this.id ? 'updated' : 'created');
          this.resetQuestion();
        }
      });
    },
    resetQuestion() {
      this.question = this.id ? null : {
        quiz_id: this.QuizId,
        question_name: null
      };

      this.typeId = 0;
      this.answers = [];
    },
    deleteAnswer(index) {
      this.$bvModal.msgBoxConfirm('Подтверждаете удаление?!', {
        title: `Удалить ответ №${index}`,
        okTitle: 'Удалить',
        centered: true,
        cancelTitle: 'Отмена',
      })
        .then(value => {
          if (value) {
            this.answers = this.answers.filter((v, i) => i !== index);
          }
        })
    },
    insertAnswer() {
      this.answers.push(Object.assign({}, this.emptyAnswer));
    },
  },
  data: () => {
    return {
      answers: [],
      question: null,
      types: [
        { value: '0', text: 'Единичный выбор' },
        { value: '3', text: 'Текст' },
        { value: '4', text: 'Множественный выбор' },
        { value: '7', text: 'Число' },
      ],
      typeId: 0,
      ownSelected: '',
      emptyAnswer: {
        AnswerText: null,
        AnswerOwn: null,
        AnswerImage: null,
        AnswerImageUrl: null,
      },
    }
  },
  created() {
    this.resetQuestion();
  },
  computed: {
    modalTitle() {
      return this.id ? 'Вопрос №' + this.id : 'Новый вопрос';
    },
    modalId() {
      return this.id ? 'modal-question-update-' + this.id : 'modal-question-new';
    },
    validation() {
      return this.question &&
        this.question.quiz_id &&
        this.question.question_name &&
        this.question.question_name.length > 3;
    },
    hideFooter() {
      return this.question ? false : true;
    }
  },
  watch: {
    ownSelected(index) {
      this.answers.forEach((answer, i) => {
        answer.AnswerOwn = (index == i);
      });
    }
  },
  props: ['QuizId', 'id', 'routes']
};
