const QuizUpdate = {
  template: `
<div>
        <b-modal @show="show" :id="'modal-quiz-update-' + id"  centered size="xl" :hide-footer="hideFooter"
                 ok-title="Сохранить" cancel-title="Отменить"
                 @ok="update"
                 :title="'Опрос №' + id">
            <div v-if="quiz">
                <p class="mb-4">
                    <!-- :state="validation" -->
                    <b-input type="text" v-model="quiz.quiz_name" />
                </p>
                <b-tabs content-class="mt-3">
                    <b-tab title="Вопросы" active>
                        <p>
                            <b-button v-b-modal.modal-question-new variant="outline-secondary">Добавить новый</b-button>
                            <questions :routes="routes" :quiz-id="quiz.quiz_id" />
                        </p>
                    </b-tab>
                    <b-tab title="Параметры">
                        <b-form-group
                                description="Несколько можно указать через запятую"
                                label="Email для получения результатов">
                            <b-form-input v-model="quiz.quiz_settings.emails" placeholder="..." ></b-form-input>
                        </b-form-group>

                        <b-form-group
                                description="Заявка с сайта"
                                label="Заголовок в Email">
                            <b-form-input v-model="quiz.quiz_settings.subject" placeholder="..." ></b-form-input>
                        </b-form-group>

                        <b-form-group
                                description="Оставьте Ваш контакт и получите подборку квартир без риска столкнуться с мошенничеством и недостроем."
                                label="Текст в контактах">
                            <b-form-textarea
                                    v-model="quiz.quiz_settings.contactsText"
                                    placeholder="..."
                                    rows="3"
                                    max-rows="6"
                            ></b-form-textarea>
                        </b-form-group>

                        <b-form-group
                                description="Спасибо"
                                label="Текст после опроса">
                            <b-form-textarea
                                    v-model="quiz.quiz_settings.resultsText"
                                    placeholder="..."
                                    rows="3"
                                    max-rows="6"
                            ></b-form-textarea>
                        </b-form-group>

                        <b-form-group
                                description="Получить результаты"
                                label="Текст кнопки получения результатов">
                            <b-form-input v-model="quiz.quiz_settings.result_button_text" placeholder="..." ></b-form-input>
                        </b-form-group>
                    </b-tab>
                </b-tabs>
            </div>
        </b-modal>
    </div>
    `,
  components: {
    Questions,
  },
  methods: {
    show() {
      this.get()
    },
    get() {
      let params = {
        id: this.id,
      };

      $.getJSON(this.routes['quizzes/get'], params).done((response) => {
        this.quiz = response;
        this.quiz.quiz_settings = this.quiz.quiz_settings ? JSON.parse(this.quiz.quiz_settings) : {};
      });
    },
    update() {
      let params = {
        id: this.id,
        quiz_name: this.quiz.quiz_name,
        quiz_settings: this.quiz.quiz_settings
      };

      $.post(this.routes['quizzes/update'], params ).done(( response ) => {
        this.$emit('updated');
        this.quiz = null;
      });
    }
  },
  data: () => {
    return {
      quiz: null
    }
  },
  computed: {
    validation() {
      return this.quiz &&
        this.quiz.quiz_name &&
        this.quiz.quiz_name.length > 3;
    },
    hideFooter() {
      return this.quiz ? false : true;
    }
  },
  props: ['id', 'routes']
};
