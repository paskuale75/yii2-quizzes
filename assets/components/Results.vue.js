const Results = {
    template: `
  <b-modal id="modal-quiz-results" @hidden="hidden" centered size="xl" hide-footer title="Результаты">
      <b-table :per-page="perPage" :current-page="currentPage" :busy="isBusy" hover striped :items="provider" :fields="fields">
        <template #table-busy>
            <div class="text-center text-danger my-2">
              <b-spinner class="align-middle"></b-spinner>
              <strong>Loading...</strong>
            </div>
        </template>
        <template #cell(show_details)="row">
            <b-button size="sm" @click="row.toggleDetails" class="mr-2">
              {{ row.detailsShowing ? 'Скрыть' : 'Показать'}}
            </b-button>
        </template>
        <template #row-details="row">
            <div v-html="row.item.quiz_results"></div>
        </template>
      </b-table>
      
      <b-pagination v-model="currentPage" :total-rows="totalRows" :per-page="perPage"></b-pagination>
  </b-modal>  
    `,
    methods: {
        hidden() {
            this.items = [];
        },
        provider(ctx, callback) {
            this.isBusy = true;
            $.post(this.routes['results/list'].
                replace('page=page', `page=${ctx.currentPage}`).
                replace('size=size', `size=${ctx.perPage}`), {}).
            done((response) => {
                this.isBusy = false;
                const items = response.items;
                this.totalRows = response.count;
                callback(items);
            });
        }
    },
    data: () => {
        return {
            totalRows: 0,
            currentPage: 1,
            perPage: 20,
            isBusy: false,
            items: [],
            fields: [
                { key: 'quiz_id', label: '№' },
                { key: 'quiz_name', label: 'Название' },
                { key: 'name', label: 'ФИО' },
                { key: 'phone', label: 'Телефон' },
                { key: 'time_taken_real', label: 'Дата' },
                { key: 'show_details', label: 'Результаты' },
            ]
        }
    },
    props: ['routes']
}