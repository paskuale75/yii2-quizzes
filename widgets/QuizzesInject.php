<?php


namespace kllakk\quizzes\widgets;


use kllakk\quizzes\models\Quizzes;

class QuizzesInject extends \yii\base\Widget
{
    public function run()
    {
        $quizzes = Quizzes::find()->all();

        return $this->render('quizzes-inject', compact('quizzes'));
    }
}