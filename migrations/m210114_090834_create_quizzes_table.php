<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%quizzes_quizzes}}`.
 */
class m210114_090834_create_quizzes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%quizzes_quizzes}}', [
            'quiz_id' => $this->primaryKey(),
            'quiz_name' => $this->text()->notNull(),
            'message_before' => $this->text(),
            'message_after' => $this->text(),
            'message_comment' => $this->text(),
            'message_end_template' => $this->text(),
            'user_email_template' => $this->text(),
            'admin_email_template' => $this->text(),
            'submit_button_text' => $this->text(),
            'name_field_text' => $this->text(),
            'business_field_text' => $this->text(),
            'email_field_text' => $this->text(),
            'phone_field_text' => $this->text(),
            'comment_field_text' => $this->text(),
            'email_from_text' => $this->text(),
            'question_answer_template' => $this->text(),
            'leaderboard_template' => $this->text(),
            'system' => $this->integer(),
            'randomness_order' => $this->integer(),
            'logged_in_user_contact' => $this->integer(),
            'show_score' => $this->integer(),
            'send_user_email' => $this->integer(),
            'send_admin_email' => $this->integer(),
            'contact_info_location' => $this->integer(),
            'user_name' => $this->integer(),
            'user_comp' => $this->integer(),
            'user_email' => $this->integer(),
            'user_phone' => $this->integer(),
            'admin_email' => $this->text(),
            'comment_section' => $this->integer(),
            'question_from_total' => $this->integer(),
            'total_user_tries' => $this->integer(),
            'total_user_tries_text' => $this->text(),
            'certificate_template' => $this->text(),
            'social_media' => $this->integer(),
            'social_media_text' => $this->text(),
            'pagination' => $this->integer(),
            'pagination_text' => $this->text(),
            'timer_limit' => $this->integer(),
            'quiz_style' => $this->text(),
            'question_numbering' => $this->integer(),
            'quiz_settings' => $this->text(),
            'theme_selected' => $this->text(),
            'last_activity' => $this->timestamp(),
            'require_log_in' => $this->integer(),
            'require_log_in_text' => $this->text(),
            'limit_total_entries' => $this->integer(),
            'limit_total_entries_text' => $this->text(),
            'scheduled_timeframe' => $this->text(),
            'scheduled_timeframe_text' => $this->text(),
            'disable_answer_onselect' => $this->integer(),
            'ajax_show_correct' => $this->integer(),
            'quiz_views' => $this->integer(),
            'quiz_taken' => $this->integer(),
            'deleted' => $this->integer(),
            'quiz_description' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%quizzes_quizzes}}');
    }
}
