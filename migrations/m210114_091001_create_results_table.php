<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%quizzes_results}}`.
 */
class m210114_091001_create_results_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%quizzes_results}}', [
            'result_id' => $this->primaryKey(),
            'quiz_id' => $this->integer()->notNull(),
            'quiz_name' => $this->string(),
            'quiz_system' => $this->integer(),
            'point_score' => $this->integer(),
            'correct_score' => $this->integer(),
            'correct' => $this->integer(),
            'total' => $this->integer(),
            'name' => $this->string(),
            'business' => $this->string(),
            'email' => $this->string(),
            'phone' => $this->string(),
            'user' => $this->integer(),
            'user_ip' => $this->string(),
            'time_taken' => $this->string(),
            'time_taken_real' => $this->timestamp(),
            'quiz_results' => $this->text(),
            'deleted' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%quizzes_results}}');
    }
}
