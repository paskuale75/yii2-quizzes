<?php

namespace kllakk\quizzes\controllers;

use kllakk\quizzes\controllers\base\BaseController;
use kllakk\quizzes\models\Conditions;
use kllakk\quizzes\models\Questions;
use Yii;
use kllakk\quizzes\models\Quizzes;
use yii\filters\VerbFilter;

class QuizzesController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'create' => ['POST'],
                    'update' => ['POST'],
                    'delete' => ['POST'],
                    'import' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDelete()
    {
        if ($id = Yii::$app->request->post('id')) {
            if ($model = Quizzes::findOne($id)) {
                if ($count = $model->delete()) {
                    return $this->asJson([$count]);
                }
            }
        }
    }

    public function actionUpdate()
    {
        if ($id = Yii::$app->request->post('id')) {
            if ($model = Quizzes::findOne($id)) {
                $model->quiz_name = Yii::$app->request->post('quiz_name');
                if ($settings = Yii::$app->request->post('quiz_settings')) {
                    $model->quiz_settings = json_encode($settings);
                }
                if ($model->save()) {
                    return $this->asJson($model);
                }
            }
        }
    }

    public function actionCreate()
    {
        if ($name = Yii::$app->request->post('quiz_name')) {
            $model = new Quizzes();
            $model->quiz_name = $name;
            if ($settings = Yii::$app->request->post('quiz_settings')) {
                $model->quiz_settings = json_encode($settings);
            }

            if ($model->save()) {
                return $this->asJson($model);
            }
        }
    }

    public function actionList()
    {
        $models = Quizzes::find()->all();
        return $this->asJson($models);
    }

    public function actionGet($id)
    {
        $model = Quizzes::findOne($id);
        return $this->asJson($model);
    }

    public function actionImport()
    {
        $content = file_get_contents($_FILES['file']['tmp_name'][0]);
        $data = json_decode($content, true);

        if ($iQuiz = $data['quizzes_quizzes'][0]) {
            $prevQuizId = $iQuiz['quiz_id'];
            $QuizName = $iQuiz['quiz_name'];
            $QuizSettings = $iQuiz['quiz_settings'];

            $quiz = new Quizzes();
            $quiz->quiz_name = $QuizName;
            $quiz->quiz_settings = $QuizSettings;
            $quiz->save();
            $quizId = $quiz->quiz_id;

            if ($quizId) {
                $prevQuestionsMap = array();
                if ($questions = ($data['quizzes_questions'] ?? null)) {
                    foreach ($questions as $iQuestion) {
                        $prevQuestionId = $iQuestion['question_id'];
                        $QuestionName = $iQuestion['question_name'];
                        $QuestionSettings = $iQuestion['question_settings'];
                        $QuestionType = $iQuestion['question_type'];
                        $AnswerArray = $iQuestion['answer_array'];
                        $QuestionOrder = $iQuestion['question_order'];

                        $question = new Questions();
                        $question->quiz_id = $quizId;
                        $question->question_name = $QuestionName;
                        $question->question_settings = $QuestionSettings;
                        $question->question_type = $QuestionType;
                        $question->answer_array = $AnswerArray;
                        $question->question_order = $QuestionOrder;
                        $question->save();
                        $questionId = $question->question_id;

                        $prevQuestionsMap [$prevQuestionId] = $questionId;
                    }
                }

                if ($conditions = ($data['quizzes_conditions'] ?? null)) {
                    foreach ($conditions as $iCondition) {
                        $condition = new Conditions();
                        $condition->quiz_id = $quizId;
                        $condition->question_id = $prevQuestionsMap[$iCondition['question_id']];

                        $ConditionArray = json_decode($iCondition['condition_array'], true);
                        foreach ($ConditionArray as &$condition_item) {
                            $condition_item['QuestionRelatedId'] = $prevQuestionsMap[$condition_item['QuestionRelatedId']];
                        }
                        $ConditionArray = json_encode($ConditionArray);

                        $condition->condition_array = $ConditionArray;
                        $condition->save();
                    }
                }
            }
        }

        return $this->asJson(['success' => isset($quizId) ? $quizId : false]);
    }

    public function actionExport($id)
    {
        $result = array();

        foreach (Quizzes::find()->andWhere(['quiz_id' => $id])->all() as $quiz) {
            $result ['quizzes_quizzes'][] = array(
                'quiz_id' => $quiz->quiz_id,
                'quiz_name' => $quiz->quiz_name,
                'quiz_settings' => $quiz->quiz_settings,
            );
        }

        foreach (Questions::find()->andWhere(['quiz_id' => $id])->all() as $question) {
            $result ['quizzes_questions'][] = array(
                'question_id' => $question->question_id,
                'quiz_id' => $question->quiz_id,
                'question_name' => $question->question_name,
                'question_settings' => $question->question_settings,
                'question_type' => $question->question_type,
                'answer_array' => $question->answer_array,
                'question_order' => $question->question_order,
            );
        }

        foreach (Conditions::find()->andWhere(['quiz_id' => $id])->all() as $condition) {
            $result ['quizzes_conditions'][] = array(
                'quiz_id' => $condition->quiz_id,
                'question_id' => $condition->question_id,
                'condition_array' => $condition->condition_array,
            );
        }

        return Yii::$app->response->sendContentAsFile(json_encode($result), "quiz-{$id}.json");
    }
}
