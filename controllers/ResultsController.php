<?php

namespace kllakk\quizzes\controllers;

use kllakk\quizzes\models\Questions;
use kllakk\quizzes\models\Quizzes;
use kllakk\quizzes\models\Results;
use Yii;
use kllakk\quizzes\controllers\base\BaseController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class ResultsController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'submit' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'only' => ['submit', 'list'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['submit'],
                        'verbs' => ['POST']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['list'],
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }

    public function actionList($page = 1, $size = 20)
    {
        $count = Results::find()->count();
        $items = Results::find()->offset(($page - 1) * $size)->limit($size)->orderBy(['time_taken_real' => SORT_DESC])->all();
        return $this->asJson(compact('items', 'count'));
    }

    public function actionSubmit()
    {
        if ($quizId = Yii::$app->request->post('qmn_quiz_id')) {
            if ($quiz = Quizzes::findOne($quizId)) {

                $sent = false;
                $quizName = $quiz->quiz_name;
                $settings = json_decode($quiz->quiz_settings);

                $qaArray = [];
                foreach ($_POST as $key => $value) {
                    if (preg_match('/^question(\d+)(_(\d+))?$/', $key, $matches)) {
                        $questionId = isset($matches[1]) ? $matches[1] : null;
                        if ($questionId) {
                            $question = Questions::findOne($questionId);
                            $QuestionName = $question->question_name;
                            $qaArray [$questionId]['name'] = $QuestionName;
                            $qaArray [$questionId]['answers'][] = $_POST[$matches[0]];
                        }
                    }
                }

                $contactName = $_POST['contact_field_0'];
                $contactPhone = $_POST['contact_field_1'];

                $resultsFormatted = $this->formatResults($quizName, $qaArray, $contactName, $contactPhone);

                $results = new Results();
                $results->quiz_id = $quizId;
                $results->quiz_name = $quizName;
                $results->name = $contactName;
                $results->phone = $contactPhone;
                $results->quiz_results = $resultsFormatted;
                $results->save();

                if ($settings->emails) {
                    if ($emailFrom = $this->module->mailFrom) {

                        $emails = array();
                        // валидация емейлов
                        foreach (explode(',', $settings->emails) as $email) {
                            $email = trim($email);
                            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                                $emails [] = $email;
                            }
                        }

                        if (count($emails) > 0) {
                            $subject = $settings->subject ? $settings->subject : $quizName;

                            $sent = Yii::$app->mailer->compose()
                                ->setHtmlBody($resultsFormatted)
                                ->setFrom($emailFrom)
                                ->setTo($emails)
                                ->setSubject($subject)
                                ->send();
                        }
                    }
                }

                $resultsText = $settings ? $settings->resultsText : 'Спасибо';
                return json_encode(array(
                    'result' => $sent,
                    'display' => '<div class="qsm-results-page"><span style="color: green">✔</span>' . $resultsText . '</div>',
                    'redirect' => false
                ));
            }
        }
    }

    private function formatResults($QuizName, $qaArray, $contactName, $contactPhone)
    {
        $date = date('d.m.Y');

        $result = '<html><body>';
        $result .= "<p>Клиент: {$contactName}</p>";
        $result .= "<p>Телефон: {$contactPhone}<p>";
        //$result .= "<p>Опрос: {$QuizName}</p>";
        $result .= "<p>Дата: {$date}</p>";

        foreach ($qaArray as $question) {

            foreach ($question['answers'] as &$answer) {
                if (preg_match('/\/quizzes\/upload\/(.*).jpg/i', $answer)) {
                    $answer = '<img style="max-width: 250px;" src="//' .  $_SERVER['SERVER_NAME'] . ($_SERVER['SERVER_PORT'] != 80 ? ':' . $_SERVER['SERVER_PORT'] : '') . $answer . '" />';
                }
            }

            $answers = implode(',', $question['answers']);
            $result .= "<p><b>{$question['name']}</b>: {$answers}</p>";
        }

        $result .= "</body></html>";

        return $result;
    }
}
