<?php

namespace kllakk\quizzes\controllers;

use kllakk\quizzes\Module;
use Yii;
use kllakk\quizzes\controllers\base\BaseController;
use kllakk\quizzes\models\Questions;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class QuestionsController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'create' => ['POST'],
                    'update' => ['POST'],
                    'delete' => ['POST'],
                    'reorder' => ['POST'],
                ],
            ],
        ];
    }

    public function actionReorder()
    {
        if (($quizId = Yii::$app->request->post('quizId')) &&
            ($questionsIds = Yii::$app->request->post('questionsIds'))
        ) {

            foreach ($questionsIds as $order => $questionId) {
                if ($question = Questions::findOne($questionId)) {
                    $question->updateAttributes(['question_order' => $order]);
                }
            }

            return $this->asJson(compact('quizId', 'questionsIds'));
        }
    }

    public function actionDelete()
    {
        if ($id = Yii::$app->request->post('id')) {
            if ($model = Questions::findOne($id)) {
                if ($count = $model->delete()) {
                    return $this->asJson([$count]);
                }
            }
        }
    }

    public function actionUpdate()
    {
        if ($id = Yii::$app->request->get('id')) {
            if ($form = Yii::$app->request->post('form')) {
                if ($model = Questions::findOne($id)) {
                    $this->updateQuestion($model, json_decode($form, true));
                }
            }
        }
    }

    public function actionCreate()
    {
        if ($form = Yii::$app->request->post('form')) {
            $model = new Questions();
            return $this->asJson($this->updateQuestion($model, json_decode($form, true)));
        }
    }

    private function updateQuestion(Questions $model, array $form)
    {
        if (isset($form['quiz_id'])) {
            if (!$model->question_id) {
                $model->question_order = 9999;
            }
            $model->quiz_id = $form['quiz_id'];
            $model->question_name = $form['question_name'];
            $model->question_type = $form['question_type'];
            $answerArray = $form['answer_array'] ?? null;
            $answerArray = $this->uploaderAnswerArray($_FILES, $answerArray);
            $model->answer_array = json_encode($answerArray);
            if ($model->save()) {
                return $model;
            }
        }

        return [];
    }

    private function uploaderAnswerArray($files, $answerArray)
    {
        $urls = array();
        foreach ($files as $indexName => $file) {
            $name = $file['name'][0];
            $tmp_name = $file['tmp_name'][0];
            $index = str_replace('file-', '', $indexName);
            $nameArray = explode('.', $name);
            $extension = end($nameArray);
            $newName = md5(uniqid(rand(), true));
            $uploads_dir = $this->module->uploadDirectory;
            $result = move_uploaded_file($tmp_name, "{$uploads_dir}/{$newName}.{$extension}");
            $urls [$index] = "{$this->module->publicDirectory}/{$newName}.{$extension}";
        }

        if ($answerArray) {
            foreach ($answerArray as $index => &$answer) {
                if (array_key_exists($index, $urls)) {
                    $answer['AnswerImage'] = null;
                    $answer['AnswerImageUrl'] = $urls[$index];
                }
            }
        }

        return $answerArray;
    }

    public function actionGet()
    {
        if ($id = Yii::$app->request->get('id')) {
            $model = Questions::findOne($id);
            return $this->asJson($model);
        } elseif ($quizId = Yii::$app->request->get('quizId')) {
            $models = Questions::find()->andWhere(['quiz_id' => $quizId])->orderBy(['question_id' => SORT_DESC])->all();
            return $this->asJson($models);
        }
    }

    public function actionList($quizId)
    {
        $models = Questions::find()->andWhere(['quiz_id' => $quizId])->all();
        return $this->asJson($models);
    }
}
