<?php

namespace kllakk\quizzes\controllers;

use kllakk\quizzes\controllers\base\BaseController;
use yii\web\Controller;

/**
 * Default controller for the `quizzes` module
 */
class DefaultController extends BaseController
{
    public $layout = 'main';

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionRoutes()
    {
        return $this->asJson($this->module->getApiRoutes());
    }

    public function actionTest()
    {
        return $this->render('index');
    }
}
