Yii2 Quizzes
=============
Quizzes module for the Yii2 framework

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist kllakk/yii2-quizzes "*"
```

or add

```
"kllakk/yii2-quizzes": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply modify your application configuration as follows:

```php
return [
    'modules' => [
        'quizzes' => [
            'class' => 'kllakk\quizzes\Module',
        ],
    ],
];
```

After you downloaded and configured Yii2-quizzess, the last thing you need to do is updating your database schema by
applying the migration:

```bash
$ php yii migrate/up --migrationPath=@kllakk/quizzes/migrations
```

You can then access Quizzes editor through the following URL:

```
http://localhost/path/to/index.php?r=quizzes/
```

Also, you need to inject quizzes templates into the main layout on your site

```php
<?= \kllakk\quizzes\widgets\QuizzesInject::widget(); ?>
```
